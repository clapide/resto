CREATE TABLE tag (                
id SERIAL  PRIMARY KEY,
name VARCHAR(100));

CREATE TABLE IF NOT EXISTS resto (
id SERIAL PRIMARY KEY,
name VARCHAR(256),
address VARCHAR(256),
url VARCHAR(256),
notes INTEGER,
id_tags INTEGER,
FOREIGN KEY (id_tags) REFERENCES TAG(id));