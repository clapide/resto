var express = require('express');
var router = express.Router();

/* GET resto listing. */
router.get('/', function(req, res, next) {
  pool.query(`SELECT * FROM resto ORDER BY id ASC`, (err, restos) => {
    if(err) console.error(err);
    restos.rows.forEach(resto => {
      pool.query(`SELECT Name
                  FROM tag WHERE id = ${resto.id}
                  ORDER BY id NAME`, function(err, tag){
        if(tag){
          console.log(tag.rows);
        }
      });
    });
    if (restos.rows){
      res.json(restos.rows);
    }
  });
});

/* GET specifique resto */
router.get('/:restoId', function(req, res, next) {
  pool.query(`SELECT *
              FROM resto
              WHERE id = ${req.params.restoId}`, (err, restos) => {
    if(err) console.error(err);
    if (restos.rows){
      restos.rows.forEach(row => {
        console.log(row);
      });
      res.json(restos.rows);
    }
  });
});

/* POST Create resto */
router.post('/', function(req, res, next) {
  if(!req.body.name && !req.body.address && !req.body.url && !req.body.notes){
    res.send('Error: missing data')
  }

  sql = `INSERT INTO resto (name, address, url, notes)
         VALUES('${req.body.name}',
                '${req.body.address}',
                '${req.body.url}',
                ${req.body.notes})
          RETURNING *`;
  pool.query(sql, (err, restos) =>  {
    if(err){
      res.send(err.message);
    } else {
      res.json(restos.rows[0].id);
    }
  });
});

/* PATCH update resto */
router.patch('/:restoId', function(req, res, next) {
  if(!req.body.name && !req.body.address && !req.body.url && !req.body.notes){
    res.send('Error: missing data')
  }

  sql = 
  `UPDATE resto SET
    name = '${req.body.name}',
    address = '${req.body.address}',
    url = '${req.body.url}',
    notes = '${req.body.notes}'
    WHERE id = ${req.params.restoId}
  RETURNING *`;
  pool.query(sql, (err, restos) => {
    if(err){
      res.send(err.message);
    } else {
      res.json(restos.rows);
    }
  });
});

module.exports = router;
