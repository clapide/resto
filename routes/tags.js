var express = require('express');
var router = express.Router();

/* GET tags listing. */
router.get('/', function(req, res, next) {
  pool.query(`SELECT * FROM tag`, (err, tags) =>{
    if(err) console.error(err);
    if (tags.rows){
      res.json(tags.rows);
    }
  });
});

/* GET specifique tags */
router.get('/:tagsId', function(req, res, next) {
  pool.query(`SELECT * FROM tag WHERE id = ${req.params.tagsId}`, (err, tags) =>{
    if(err) console.error(err);
    if (tags.rows){
      tags.rows.forEach(row => {
        console.log(row);
      });
      res.json(tags.rows);
    }
  });
});

/* POST Create tags */
router.post('/', function(req, res, next) {
  if(!req.body.name){
    res.send('Error: missing data')
  }

  sql = `INSERT INTO tag (name) VALUES('${req.body.name}') RETURNING *`;
  pool.query(sql, (err, tags) => {
    if(err){
      res.send(err.message);
    } else {
      res.json(tags.rows[0].id);
    }
  });
});

/* PATCH update tags */
router.patch('/:tagsId', function(req, res, next) {
  if(!req.body.name){
    res.send('Error: missing data')
  }

  sql = `UPDATE tag SET name = '${req.body.name}'
         WHERE id = ${req.params.tagsId}
         RETURNING *`;
  pool.query(sql, (err, tags) => {
    if(err){
      res.send(err.message);
    } else {
      res.json(tags.rows);
    }
  });
});

module.exports = router;
